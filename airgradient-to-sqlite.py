#!/usr/bin/python3

"""
This script fetches the JSON data from an airgradient device and stores it as
a time series in a sqlite database
"""

import argparse
import json
import pathlib
import pickle
import sqlite3
import sys
import time

from datetime import datetime

import requests


def parse_args(sys_args):  # pylint: disable=W0613
    """CLI argument comprehension"""
    example_text = '''example:
        airgradient-to-sqlite -d "/path/to/db.sqlite3" -u "http://1.1.1.1/json"'''
    parser = argparse.ArgumentParser(prog='airgradient-to-sqlite',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=__doc__,
                                     epilog=example_text)
    parser.add_argument('-d', '--db', type=pathlib.Path, required=True,
                        help='path to the sqlite database')
    parser.add_argument('-o', '--output', type=pathlib.Path,
                        help='optional pickle object including the latest data')
    parser.add_argument('-u', '--url', type=str, required=True,
                        help='URL for the data.json file')
    return parser.parse_args()


def fetch_data(url):
    """Fetch JSON data from airgradient device"""
    json_data = requests.get(url, timeout=5)
    return json_data


def json_to_dict(json_data):
    """Process JSON data from airgradient device and output a dict"""
    processed_data = {}
    processed_data['date'] = time.mktime(datetime.now().timetuple())
    json_obj = json.loads(json_data.text)
    for value in json_obj:
        if value == 'co2':
            processed_data['co2'] = json_obj['co2']
        elif value == 'pm':
            processed_data['pm2_5'] = json_obj['pm']
        elif value == 'temp':
            processed_data['temperature'] = json_obj['temp']
        elif value == 'humid':
            processed_data['humidity'] = json_obj['humid']
        elif value == 'voc':
            processed_data['tvoc'] = json_obj['voc']
        elif value == 'nox':
            processed_data['nox'] = json_obj['nox']
    return processed_data


def sqlite_create_table(cursor):
    """Create sqlite table if it does not exist"""
    create_table = """
    CREATE TABLE IF NOT EXISTS airgradient(
        date REAL,
        co2 REAL,
        pm2_5 REAL,
        temperature REAL,
        humidity REAL,
        tvoc REAL,
        nox REAL)"""
    cursor.execute(create_table)


def sqlite_import(db, processed_data):
    """Import data into the sqlite database"""
    con = sqlite3.connect(db)
    cur = con.cursor()
    sqlite_create_table(cur)
    cur.execute("""
    INSERT INTO airgradient
        (date, co2, pm2_5, temperature, humidity, tvoc, nox)
    VALUES
        (:date, :co2, :pm2_5, :temperature, :humidity, :tvoc, :nox)""",
        processed_data)
    con.commit()
    con.close()


def pickle_export(processed_data, output):
    """Export data to a pickle, to be used in other scripts"""
    with open(output, 'wb') as pout:
        pickle.dump(processed_data, pout)


def main():
    """Main function."""
    args = parse_args(sys.argv[1:])
    json_data = fetch_data(args.url)
    processed_data = json_to_dict(json_data)
    sqlite_import(args.db, processed_data)
    pickle_export(processed_data, args.output)


if __name__ == "__main__":
    main()
